\babel@toc {german}{}
\contentsline {section}{\numberline {1}Einf\IeC {\"u}hrung}{2}{section.1}% 
\contentsline {section}{\numberline {2}Aufbau und Notation des W\IeC {\"u}rfels}{2}{section.2}% 
\contentsline {subsection}{\numberline {2.1}Aufbau}{2}{subsection.2.1}% 
\contentsline {subsection}{\numberline {2.2}Anzahl der m\IeC {\"o}glichen Stellungen des 3x3x3 Rubik's Cubes}{4}{subsection.2.2}% 
\contentsline {section}{\numberline {3}Der Rubik's Cube als Gruppe}{6}{section.3}% 
\contentsline {subsection}{\numberline {3.1}Gruppe allgemein}{6}{subsection.3.1}% 
\contentsline {subsection}{\numberline {3.2}Die Rubik's Gruppe}{6}{subsection.3.2}% 
\contentsline {section}{\numberline {4}Untergruppen}{7}{section.4}% 
\contentsline {subsection}{\numberline {4.1}Untergruppen allgemein}{7}{subsection.4.1}% 
\contentsline {subsection}{\numberline {4.2}Untergruppen am Rubik's Cube}{7}{subsection.4.2}% 
\contentsline {section}{\numberline {5}Satz von Lagrange}{9}{section.5}% 
\contentsline {section}{\numberline {6}Cayley Graphs}{11}{section.6}% 
\contentsline {section}{\numberline {7}Makros}{13}{section.7}% 
\contentsline {subsection}{\numberline {7.1}Kommutatoren}{13}{subsection.7.1}% 
\contentsline {subsection}{\numberline {7.2}Konjugation}{14}{subsection.7.2}% 
\contentsline {section}{\numberline {8}Beispiel}{14}{section.8}% 
\contentsline {section}{\numberline {9}Fazit}{17}{section.9}% 
